//
//  DataViewController.swift
//  G63L14_3
//
//  Created by Ivan Vasilevich on 7/15/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class DataViewController: UIViewController {

	@IBOutlet weak var dataLabel: UILabel!
	var dataObject: String = ""

	@IBOutlet weak var imageView: UIImageView!
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		let stringFromCode = NSLocalizedString("Vasya_Privet", comment: "Pirivetstvie Vasiliya")
		self.dataLabel!.text = dataObject + stringFromCode
		self.imageView.image = UIImage(named: dataObject)
	}


}

