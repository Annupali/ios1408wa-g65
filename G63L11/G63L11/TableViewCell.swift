//
//  TableViewCell.swift
//  G63L11
//
//  Created by Ivan Vasilevich on 7/7/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
		backgroundView?.backgroundColor = .orange
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
