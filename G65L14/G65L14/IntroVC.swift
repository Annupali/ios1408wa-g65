//
//  IntroVC.swift
//  G65L14
//
//  Created by Ivan Vasilevich on 9/27/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class IntroVC: UIViewController {

	@IBOutlet weak var scrollView: UIScrollView!
	@IBOutlet weak var pageControl: UIPageControl!
	
	let screensCount: CGFloat = 3
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		scrollView.contentSize = CGSize(width: scrollView.frame.size.width * screensCount, height: scrollView.frame.size.height)
		placeImages()
	}
	
	func placeImages() {
		
		for i in 0..<Int(screensCount) {
			let box = UIImageView.init(frame: CGRect(x: CGFloat(i) * scrollView.frame.width , y: 0, width: scrollView.frame.width, height: scrollView.frame.height))
			box.clipsToBounds = true
			box.contentMode = .scaleToFill
			box.image = UIImage(named: i.description)
			scrollView.addSubview(box)
//			scrollView.frame.size.width = 55
//			scrollView.frame.width = 55
		}
	}
    
	@IBAction func pageChanged(_ sender: UIPageControl) {
		scrollView.setContentOffset(CGPoint(x: CGFloat(sender.currentPage) * scrollView.frame.width, y: 0), animated: true)
	}
	
}

extension IntroVC : UIScrollViewDelegate {
//	func scrollViewDidScroll(_ scrollView: UIScrollView) {
//		
//	}
	
	func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
		pageControl.currentPage = Int(scrollView.contentOffset.x / scrollView.frame.width)
	}
}
